#!/usr/bin/env nextflow

//* Preproc *//
include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'
include { procd_fqs_to_alns } from '../alignment/alignment.nf'

//* CNA tools *//
// Sequenza
include { sequenza_gc_wiggle } from '../sequenza/sequenza.nf'
include { sequenza_bam2seqz } from '../sequenza/sequenza.nf'
include { sequenza_seqz_binning } from '../sequenza/sequenza.nf'
include { sequenza_extract } from '../sequenza/sequenza.nf'
include { sequenza_fit }  from '../sequenza/sequenza.nf'
include { sequenza_result } from '../sequenza/sequenza.nf'

// CNVkit
include { cnvkit_batch } from '../cnvkit/cnvkit.nf'
include { cnvkit_guess_baits } from '../cnvkit/cnvkit.nf'
include { cnvkit_autobin } from '../cnvkit/cnvkit.nf'
include { cnvkit_coverage as cnvkit_coverage_target } from '../cnvkit/cnvkit.nf'
include { cnvkit_coverage as cnvkit_coverage_antitarget } from '../cnvkit/cnvkit.nf'
include { cnvkit_reference } from '../cnvkit/cnvkit.nf'
include { cnvkit_fix } from '../cnvkit/cnvkit.nf'
include { cnvkit_segment } from '../cnvkit/cnvkit.nf'
include { cnvkit_call } from '../cnvkit/cnvkit.nf'
include { cnvkit_scatter as cnvkit_scatter_cnr } from '../cnvkit/cnvkit.nf'
include { cnvkit_scatter as cnvkit_scatter_cns } from '../cnvkit/cnvkit.nf'
include { cnvkit_scatter_cns_cnr } from '../cnvkit/cnvkit.nf'

//* Tumor purity tools *//
// tidyestimate
include { tidyestimate } from '../tidyestimate/tidyestimate.nf'

//* CCF tools *//
// PyClone-VI
include { lenstools_make_pyclonevi_inputs } from '../lenstools/lenstools.nf'
include { pyclonevi_fit } from '../pyclone-vi/pyclone-vi.nf'
include { pyclonevi_write_results_file } from '../pyclone-vi/pyclone-vi.nf'

//* HLA LOH tools *//
// LOHHLA
include { convert_alleles_to_lohhla_format } from '../lohhla/lohhla.nf'
include { lohhla } from '../lohhla/lohhla.nf'

// Misc.
include { tximport } from '../tximport/tximport.nf'
include { alns_to_transcript_counts } from '../rna_quant/rna_quant.nf'
include { gtfparse_tx_id_to_gene_name } from '../gtfparse/gtfparse.nf'
include { samtools_index } from '../samtools/samtools.nf'
include { samtools_sort } from '../samtools/samtools.nf'
include { samtools_view } from '../samtools/samtools.nf'

// Cleaning
include { clean_work_files as clean_trimmed_fastqs } from '../utilities/utilities.nf'


workflow manifest_to_cnas {
// require:
//   MANIFEST
//   VCFS
//   params.onco$manifest_to_cnas$fq_trim_tool
//   params.onco$manifest_to_cnas$fq_trim_tool_parameters
//   params.onco$manifest_to_cnas$aln_tool
//   params.onco$manifest_to_cnas$aln_tool_parameters
//   params.onco$manifest_to_cnas$cna_tool
//   params.onco$manifest_to_cnas$cna_tool_refs
//   params.onco$manifest_to_cnas$cna_tool_parameters
//   params.onco$manifest_to_cnas$aln_ref
//   params.onco$manifest_to_cnas$bed
//   params.onco$manifest_to_cnas$gtf
  take:
    manifest
    vcfs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_refs
    cna_tool_parameters
    aln_ref
    bed
    gtf
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_cnas(
      manifest_to_raw_fqs.out.fqs,
      vcfs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      cna_tool,
      cna_tool_refs,
      cna_tool_parameters,
      aln_ref,
      bed,
      gtf,
      manifest)
  emit:
    alns = raw_fqs_to_cnas.out.alns
    cnas = raw_fqs_to_cnas.out.cnas
}

workflow raw_fqs_to_cnas {
// require:
//   FQS
//   VCFS
//   params.onco$raw_fqs_to_cnas$fq_trim_tool
//   params.onco$raw_fqs_to_cnas$fq_trim_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_tool
//   params.onco$raw_fqs_to_cnas$aln_tool_parameters
//   params.onco$raw_fqs_to_cnas$cna_tool
//   params.onco$raw_fqs_to_cnas$cna_tool_refs
//   params.onco$raw_fqs_to_cnas$cna_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_ref
//   params.onco$raw_fqs_to_cnas$bed
//   params.onco$raw_fqs_to_cnas$gtf
//   MANIFEST
  take:
    fqs
    vcfs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_refs
    cna_tool_parameters
    aln_ref
    bed
    gtf
    manifest
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_cnas(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      vcfs,
      aln_tool,
      aln_tool_parameters,
      cna_tool,
      cna_tool_refs,
      cna_tool_parameters,
      aln_ref,
      bed,
      gtf,
      manifest)
  emit:
    alns = procd_fqs_to_cnas.out.alns
    cnas = procd_fqs_to_cnas.out.cnas
}

workflow procd_fqs_to_cnas {
// require:
//   PROCD_FQS
//   VCFS
//   params.onco$raw_fqs_to_cnas$aln_tool
//   params.onco$raw_fqs_to_cnas$aln_tool_parameters
//   params.onco$raw_fqs_to_cnas$cna_tool
//   params.onco$raw_fqs_to_cnas$cna_tool_refs
//   params.onco$raw_fqs_to_cnas$cna_tool_parameters
//   params.onco$raw_fqs_to_cnas$aln_ref
//   params.onco$raw_fqs_to_cnas$bed
//   params.onco$raw_fqs_to_cnas$gtf
//   MANIFEST
  take:
    procd_fqs
    vcfs
    aln_tool
    aln_tool_parameters
    cna_tool
    cna_tool_refs
    cna_tool_parameters
    aln_ref
    bed
    gtf
    manifest
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      params.dummy_file,
      '')
    // Cleaning upstream input intermediates
    procd_fqs
      .concat(procd_fqs_to_alns.out.alns)
      .groupTuple(by: [0, 1, 2], size: 2)
      .flatten()
      .filter{ it =~ /trimmed.fastq.gz$|trimmed.fq.gz$/ }
      .set { procd_fqs_done_signal }
//    clean_trimmed_fastqs(
//      procd_fqs_done_signal)
    alns_to_cnas(
      procd_fqs_to_alns.out.alns,
      vcfs,
      cna_tool,
      cna_tool_refs,
      cna_tool_parameters,
      aln_ref,
      bed,
      gtf,
      manifest)
  emit:
    alns = procd_fqs_to_alns.out.alns
    cnas = alns_to_cnas
}


workflow alns_to_cnas {
  take:
    alns
    vcfs
    cna_tool
    cna_tool_refs
    cna_tool_parameters
    aln_ref
    bed
    gtf
    manifest
  main:
    cnas = Channel.empty()
    samtools_index(
      alns,
      '')
    //A lot of filtering, joining, etc. to ensure inputs go in as expected.
    manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
    manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
    alns
      .join(samtools_index.out.bais, by: [0, 1])
      .set{ bams_bais }
    bams_bais
      .join(norms, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ norm_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ tumor_bams_bais }
    norm_bams_bais
      .join(tumor_bams_bais, by: [0, 2])
      .set{ norm_tumor_bams_bais }
    cna_tool_parameters = Eval.me(cna_tool_parameters)
    cna_tool_refs = Eval.me(cna_tool_refs)
    if( cna_tool =~ /sequenza/ ) {
      // Message
      if( params.prnt_docs ) {
        println "[raft]"
        println "[raft]CNA Tool: Sequenza"
        println "[raft]Sequenza has multiple steps:"
        println "[raft]  sequenza_gc_wiggle"
        println "[raft]  sequenza_bam2seqz"
        println "[raft]  sequenza_seqz_binning"
        println "[raft]Use a hash to define parameters for each step (e.g."
        println "[raft]cna_tool_parameters = \"[sequenza_gc_wiggle:'foo', sequenza_bam2seqz:'bar']\""
      }
      // Parameter parsing
      sequenza_gc_wiggle_parameters = cna_tool_parameters['sequenza_gc_wiggle'] ? cna_tool_parameters['sequenza_gc_wiggle'] : '-w 50'
      sequenza_bam2seqz_parameters = cna_tool_parameters['sequenza_bam2seqz'] ? cna_tool_parameters['sequenza_bam2seqz'] : ''
      sequenza_seqz_binning_parameters = cna_tool_parameters['sequenza_seqz_binning'] ? cna_tool_parameters['sequenza_seqz_binning'] : '-w 50'
      // Execution
      //A lot of filtering, joining, etc. to ensure inputs go in as expected.
      sequenza_gc_wiggle(
        aln_ref,
        sequenza_gc_wiggle_parameters)
      sequenza_bam2seqz(
         norm_tumor_bams_bais,
         sequenza_gc_wiggle.out.gc_wig,
         bed,
         sequenza_bam2seqz_parameters)
      sequenza_seqz_binning(
        sequenza_bam2seqz.out.seqz,
        sequenza_seqz_binning_parameters)
      sequenza_extract(
        sequenza_seqz_binning.out.small_seqz)
      sequenza_fit(
        sequenza_extract.out.extracts)
      sequenza_result(
        sequenza_fit.out.fits)
      sequenza_result.out.results.set{ cnas }
    }
    if( cna_tool =~ /cnvkit_batch/ ) {
      cnvkit_batch_parameters = cna_tool_parameters['cnvkit_batch'] ? cna_tool_parameters['cnvkit_batch'] : ''
      cnvkit_batch(
        norm_tumor_bams_bais,
        bed,
        aln_ref,
        cnvkit_batch_parameters)
    }
    if( cna_tool =~ /cnvkit/ ) {
      alns
        .map{ it[3] }
        .collect()
        .set{ joint_bams }
      alns
        .filter{ it[1] =~ /nd-/ }
        .map{ it[3] }
        .collect()
        .set{ joint_norm_bams }
      cnvkit_autobin_parameters = cna_tool_parameters['cnvkit_autobin'] ? cna_tool_parameters['cnvkit_autobin'] : ''
      cnvkit_coverage_parameters = cna_tool_parameters['cnvkit_coverage'] ? cna_tool_parameters['cnvkit_coverage'] : ''
      cnvkit_reference_parameters = cna_tool_parameters['cnvkit_reference'] ? cna_tool_parameters['cnvkit_reference'] : ''
      cnvkit_fix_parameters = cna_tool_parameters['cnvkit_fix'] ? cna_tool_parameters['cnvkit_fix'] : ''
      cnvkit_segment_parameters = cna_tool_parameters['cnvkit_segment'] ? cna_tool_parameters['cnvkit_segment'] : ''
      cnvkit_call_parameters = cna_tool_parameters['cnvkit_call'] ? cna_tool_parameters['cnvkit_call'] : ''
      cnvkit_scatter_cnr_parameters = cna_tool_parameters['cnvkit_scatter_cnr'] ? cna_tool_parameters['cnvkit_scatter_cnr'] : ''
      cnvkit_scatter_cns_parameters = cna_tool_parameters['cnvkit_scatter_cns'] ? cna_tool_parameters['cnvkit_scatter_cns'] : ''
      cnvkit_scatter_cns_cnr_parameters = cna_tool_parameters['cnvkit_scatter_cns_cnr'] ? cna_tool_parameters['cnvkit_scatter_cns_cnr'] : ''
      cnvkit_guess_baits(
        joint_norm_bams,
        bed)
      cnvkit_autobin(
        joint_norm_bams,
        gtf,
        cnvkit_guess_baits.out.baits_3col_bed,
        cnvkit_autobin_parameters)
      cnvkit_coverage_target(
        alns,
        cnvkit_autobin.out.targets_bed,
        cnvkit_coverage_parameters)
      cnvkit_coverage_antitarget(
        alns,
        cnvkit_autobin.out.antitargets_bed,
        cnvkit_coverage_parameters)
      // Only want normals for making the refernce.cnn
      cnvkit_coverage_target.out.cnns
        .filter{ it[1] =~ /nd-/ }
        .map{ it[3] }
        .collect()
        .set{ joint_norm_target_cnns }
      cnvkit_coverage_antitarget.out.cnns
        .filter{ it[1] =~ /nd-/ }
        .map{ it[3] }
        .collect()
        .set{ joint_norm_antitarget_cnns }
      cnvkit_reference(
        joint_norm_target_cnns,
        joint_norm_antitarget_cnns,
        aln_ref,
        cnvkit_reference_parameters)
      // Only want tumors for segmentation
      cnvkit_coverage_target.out.cnns
        .filter{ it[1] =~ /ad-/ }
        .set{ tumor_target_cnns }
      cnvkit_coverage_antitarget.out.cnns
        .filter{ it[1] =~ /ad-/ }
        .set{ tumor_antitarget_cnns }
      tumor_target_cnns
        .join(tumor_antitarget_cnns, by: [0, 1, 2])
        .set{ tumor_cnns }
      cnvkit_fix(
        tumor_cnns,
        cnvkit_reference.out.reference_cnn,
        cnvkit_fix_parameters)
      cnvkit_segment(
        cnvkit_fix.out.cnrs,
        cnvkit_segment_parameters)
      //Combining CNSs with VCFs. Need to add purity here as well.
      vcfs
        .map{ [it[0], it[2], it[3], it[4]] }
        .join(cnvkit_segment.out.cnss, by: [0, 1, 2])
        .set{ cnvkit_call_inputs }
      cnvkit_call(
//        cnvkit_segment.out.cnss,
        cnvkit_call_inputs,
        cnvkit_call_parameters)
      cnvkit_scatter_cnr(
        cnvkit_fix.out.cnrs,
        cnvkit_scatter_cnr_parameters)
      cnvkit_scatter_cns(
        cnvkit_segment.out.cnss,
        cnvkit_scatter_cns_parameters)
      cnvkit_segment.out.cnss
        .join(cnvkit_fix.out.cnrs, by: [0, 1, 2])
        .set{ cnss_and_cnrs }
      cnvkit_scatter_cns_cnr(
        cnss_and_cnrs,
        cnvkit_scatter_cns_cnr_parameters)
    }
    if( cna_tool =~ /accucopy/ ) {
      accucopy_parameters = cna_tool_parameters['accucopy'] ? cna_tool_parameters['accucopy'] : ''
      accucopy_ref = cna_tool_refs['accucopy'] ? cna_tool_refs['accucopy'] : ''
      accucopy(
        norm_tumor_bams_bais,
        accucopy_ref,
        accucopy_parameters)
    }
    if( cna_tool =~ /hificnv/ ) {
      hificnv_parameters = cna_tool_parameters['hificnv'] ? cna_tool_parameters['hificnv'] : ''
      hificnv(
        tumor_bams_bais,
        aln_ref,
        hificnv_parameters)
    }
  emit:
    cnas = cnvkit_call.out.call_cnss
}

workflow cnas_and_vcfs_to_ccfs {
  take:
    cnas
    cna_tool
    targ_vcfs
    af_vcfs
    ccf_tool
    ccf_tool_parameters
  main:
    ccf_tool_parameters = Eval.me(ccf_tool_parameters)
    if( ccf_tool  =~ /pyclone-vi/ ) {
      if ( cna_tool =~ /sequenza/ ) {
        lenstools_make_pyclonevi_inputs_parameters = ccf_tool_parameters['lenstools_make_pyclonevi_input_parameters'] ? ccf_tool_parameters['lenstools_make_pyclonevi_input_parameters'] : ''
        pyclonevi_fit_parameters = ccf_tool_parameters['pyclonevi_fit'] ? ccf_tool_parameters['pyclonevi_fit'] : ''
        pyclonevi_write_results_file_parameters = ccf_tool_parameters['pyclonevi_write_results_file'] ? ccf_tool_parameters['pyclonevi_write_results_file'] : ''

        targ_vcfs
          .join(af_vcfs, by:[0,1, 2, 3])
          .join(cnas, by: [0, 1, 2, 3])
          .set{ joint_pcvi }

        lenstools_make_pyclonevi_inputs(
          joint_pcvi,
          lenstools_make_pyclonevi_inputs_parameters)

        pyclonevi_fit(
          lenstools_make_pyclonevi_inputs.out.pcvi_inputs,
          pyclonevi_fit_parameters)

        pyclonevi_write_results_file(
          pyclonevi_fit.out.pcvi_tmps,
          pyclonevi_write_results_file_parameters)
      }
    }
  emit:
    ccfs = pyclonevi_write_results_file.out.pcvi_results
}

workflow manifest_to_tumor_purities {
// require:
//   MANIFEST
//   params.onco$manifest_to_tumor_purities$fq_trim_tool
//   params.onco$manifest_to_tumor_purities$fq_trim_tool_parameters
//   params.onco$manifest_to_tumor_purities$aln_tool
//   params.onco$manifest_to_tumor_purities$aln_tool_parameters
//   params.onco$manifest_to_tumor_purities$tx_quant_tool
//   params.onco$manifest_to_tumor_purities$tx_quant_tool_parameters
//   params.onco$manifest_to_tumor_purities$tumor_purities_tool
//   params.onco$manifest_to_tumor_purities$tumor_purities_tool_parameters
//   params.onco$manifest_to_tumor_purities$aln_ref
//   params.onco$manifest_to_tumor_purities$gtf
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    tx_quant_tool
    tx_quant_tool_parameters
    tumor_purity_tool
    tumor_purity_tool_parameters
    aln_ref
    gtf
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_tumor_purities(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      tx_quant_tool,
      tx_quant_tool_parameters,
      tumor_purity_tool,
      tumor_purity_tool_parameters,
      aln_ref,
      gtf)
  emit:
    tumor_purities = raw_fqs_to_tumor_purities.out.tumor_purities
}


workflow raw_fqs_to_tumor_purities {
// require:
//   FQS
//   params.onco$raw_fqs_to_tumor_purities$fq_trim_tool
//   params.onco$raw_fqs_to_tumor_purities$fq_trim_tool_parameters
//   params.onco$raw_fqs_to_tumor_purities$aln_tool
//   params.onco$raw_fqs_to_tumor_purities$aln_tool_parameters
//   params.onco$raw_fqs_to_tumor_purities$tx_quant_tool
//   params.onco$raw_fqs_to_tumor_purities$tx_quant_tool_parameters
//   params.onco$raw_fqs_to_tumor_purities$tumor_purities_tool
//   params.onco$raw_fqs_to_tumor_purities$tumor_purities_tool_parameters
//   params.onco$raw_fqs_to_tumor_purities$aln_ref
//   params.onco$raw_fqs_to_tumor_purities$gtf
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    tx_quant_tool
    tx_quant_tool_parameters
    tumor_purity_tool
    tumor_purity_tool_parameters
    aln_ref
    gtf
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_tumor_purities(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      tx_quant_tool,
      tx_quant_tool_parameters,
      tumor_purity_tool,
      tumor_purity_tool_parameters,
      aln_ref,
      gtf)
  emit:
    tumor_purities = procd_fqs_to_tumor_purities.out.tumor_purities
}

workflow procd_fqs_to_tumor_purities {
// require:
//   PROCD_FQS
//   params.onco$procd_fqs_to_tumor_purities$aln_tool
//   params.onco$procd_fqs_to_tumor_purities$aln_tool_parameters
//   params.onco$procd_fqs_to_tumor_purities$tx_quant_tool
//   params.onco$procd_fqs_to_tumor_purities$tx_quant_tool_parameters
//   params.onco$procd_fqs_to_tumor_purities$tumor_purities_tool
//   params.onco$procd_fqs_to_tumor_purities$tumor_purities_tool_parameters
//   params.onco$procd_fqs_to_tumor_purities$aln_ref
//   params.onco$procd_fqs_to_tumor_purities$gtf
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    tx_quant_tool
    tx_quant_tool_parameters
    tumor_purity_tool
    tumor_purity_tool_parameters
    aln_ref
    gtf
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      gtf,
      '')
    // Cleaning upstream input intermediates
    procd_fqs
      .concat(procd_fqs_to_alns.out.alns)
      .groupTuple(by: [0, 1, 2], size: 2)
      .flatten()
      .filter{ it =~ /trimmed.fastq.gz$|trimmed.fq.gz$/ }
      .set { procd_fqs_done_signal }
    clean_trimmed_fastqs(
      procd_fqs_done_signal)
    alns_to_tumor_purities(
      procd_fqs_to_alns.out.alt_alns,
      tx_quant_tool,
      tx_quant_tool_parameters,
      tumor_purity_tool,
      tumor_purity_tool_parameters,
      aln_ref,
      gtf)
  emit:
    tumor_purities = alns_to_tumor_purities.out.tumor_purities
}

workflow alns_to_tumor_purities {
// require:
//   ALNS
//   params.onco$alns_to_tumor_purities$tx_quant_tool
//   params.onco$alns_to_tumor_purities$tx_quant_tool_parameters
//   params.onco$alns_to_tumor_purities$tumor_purities_tool
//   params.onco$alns_to_tumor_purities$tumor_purities_tool_parameters
//   params.onco$alns_to_tumor_purities$aln_ref
//   params.onco$alns_to_tumor_purities$gtf
//   params.onco$alns_to_tumor_purities$bed
//   manifest
  take:
    alns
    tx_quant_tool
    tx_quant_tool_parameters
    tumor_purity_tool
    tumor_purity_tool_parameters
    aln_ref
    gtf
    bed
    manifest
  main:
    tumor_purities = Channel.empty()
    tumor_purity_tool_parameters = Eval.me(tumor_purity_tool_parameters)
    if( tumor_purity_tool =~ /sequenza/ ) {
      samtools_index(
        alns,
        '')
      //A lot of filtering, joining, etc. to ensure inputs go in as expected.
      manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
      manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
      alns
        .join(samtools_index.out.bais, by: [0, 1])
        .set{ bams_bais }
      bams_bais
        .join(norms, by: [0, 1])
        .map{ [it[0], it[1], it[2], it[3], it[5]] }
        .set{ norm_bams_bais }
      bams_bais
        .join(tumors, by: [0, 1])
        .map{ [it[0], it[1], it[2], it[3], it[5]] }
        .set{ tumor_bams_bais }
      norm_bams_bais
        .join(tumor_bams_bais, by: [0, 2])
        .set{ norm_tumor_bams_bais }

      sequenza_gc_wiggle_parameters = tumor_purity_tool_parameters['sequenza_gc_wiggle'] ? tumor_purity_tool_parameters['sequenza_gc_wiggle'] : '-w 50'
      sequenza_bam2seqz_parameters = tumor_purity_tool_parameters['sequenza_bam2seqz'] ? tumor_purity_tool_parameters['sequenza_bam2seqz'] : ''
      sequenza_seqz_binning_parameters = tumor_purity_tool_parameters['sequenza_seqz_binning'] ? tumor_purity_tool_parameters['sequenza_seqz_binning'] : '-w 50'
      sequenza_gc_wiggle(
        aln_ref,
        sequenza_gc_wiggle_parameters)
      sequenza_bam2seqz(
         norm_tumor_bams_bais,
         sequenza_gc_wiggle.out.gc_wig,
         bed,
         sequenza_bam2seqz_parameters)
      sequenza_seqz_binning(
        sequenza_bam2seqz.out.seqz,
        sequenza_seqz_binning_parameters)
      sequenza_extract(
        sequenza_seqz_binning.out.small_seqz)
      sequenza_fit(
        sequenza_extract.out.extracts)
      sequenza_result(
        sequenza_fit.out.fits)
      sequenza_result.out.results.set{ tumor_purities }
    }
//    if( tumor_purity_tool =~ /estimate|tidyestimate/ ) {
//      alns_to_transcript_counts(
//        alns,
//       aln_ref,
//        gtf,
//        tx_quant_tool,
//        tx_quant_tool_parameters)
//      quants_to_tumor_purities(
//        alns_to_transcript_counts.out.quants,
//        tumor_purity_tool,
//        tumor_purity_tool_parameters,
//        gtf)
//    }
  emit:
//    tumor_purities = quants_to_tumor_purities.out.tumor_purities
    tumor_purities
}

//workflow quants_to_tumor_purities {
//// require:
////   QUANTS
////   params.onco$quants_to_tumor_purities$tumor_purities_tool
////   params.onco$quants_to_tumor_purities$tumor_purities_tool_parameters
////   params.onco$quants_to_tumor_purities$gtf
//  take:
//    quants
//    tumor_purity_tool
//    tumor_purity_tool_parameters
//    gtf
//  main:
//    tumor_purity_tool_parameters = Eval.me(tumor_purity_tool_parameters)
//    if( tumor_purity_tool =~ /estimate|tidyestimate/ ) {
//      tximport_type = tumor_purity_tool_parameters['tximport_type'] ? tumor_purity_tool_parameters['tximport_type'] : ''
//      gtfparse_tx_id_to_gene_name(
//        gtf)
//      tximport(
//        quants,
//        gtfparse_tx_id_to_gene_name.out.tx2gene,
//        tximport_type,
//        'tx2gene.tsv')
//      tidyestimate(
//        tximport.out.gene_count_mtxs)
//    }
//  emit:
//    tumor_purities = tidyestimate.out.tumor_purities
//}


// Need to add upstream workflows for this one.
workflow alns_and_alleles_to_hla_loh {
// require:
//   MANIFEST
//   ALNS
//   ALLELES
//   params.onco$alns_and_alleles_to_loh_hla$hla_loh_tool
//   params.onco$alns_and_alleles_to_loh_hla$hla_loh_tool_refs
//   params.onco$alns_and_alleles_to_loh_hla$hla_loh_tool_parameters
  take:
    manifest
    alns
    alleles
    hla_loh_tool
    hla_loh_tool_refs
    hla_loh_tool_parameters
  main:
    samtools_index(
      alns,
      '')
    //A lot of filtering, joining, etc. to ensure inputs go in as expected.
    manifest.filter{ it[5] =~ /TRUE/ }.set{ norms }
    manifest.filter{ it[5] =~ /FALSE/ }.set{ tumors }
    alns
      .join(samtools_index.out.bais, by: [0, 1])
      .set{ bams_bais }
    bams_bais
      .join(norms, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ norm_bams_bais }
    bams_bais
      .join(tumors, by: [0, 1])
      .map{ [it[0], it[1], it[2], it[3], it[5]] }
      .set{ tumor_bams_bais }
    norm_bams_bais
      .join(tumor_bams_bais, by: [0, 2])
      .set{ norm_tumor_bams_bais }


    hla_loh = ''
    hla_loh_tool_refs = Eval.me(hla_loh_tool_refs)
    hla_loh_tool_parameters = Eval.me(hla_loh_tool_parameters)
    if( hla_loh_tool  =~ /lohhla/ ) {
      lohhla_ref = hla_loh_tool_refs['lohhla']
      convert_alleles_to_lohhla_format(
        alleles)
      norm_tumor_bams_bais
        .join(convert_alleles_to_lohhla_format.out.lohhla_alleles.map{ [it[0], it[3]] }, by: 0)
        .set{ norm_tumor_bams_bais_alleles }
      lohhla(
        norm_tumor_bams_bais_alleles,
        lohhla_ref)

    }
  emit:
    hla_loh
}
